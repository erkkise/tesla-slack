const _ = require('lodash')
const geolib = require('geolib')
const knownPlaces = require('./known-places')
const Tesla = require('tesla-api')

function listVehicles(email, password) {
  return Tesla.login({email, password, distanceUnit: 'km'})
}

async function vehicleWakeUp(vehicle) {
  await vehicle.wakeUp()
}

async function vehicleInfo(vehicle) {
  await vehicleWakeUp(vehicle);
  const battery = await vehicleBattery(vehicle)
  const speedAndLocation = await vehicleSpeedAndLocation(vehicle)
  const climate = await climateState(vehicle)
  return battery.concat(speedAndLocation).concat(climate)
}

async function vehicleSpeedAndLocation(vehicle) {
  const state = await vehicle.driveState()
  return [`Speed: ${state.speed || 0} km/h`,
	  `Location: ${location(state)}`]
}

async function vehicleBattery(vehicle) {
  const state = await vehicle.chargeState()
  const range = `Current range is ${state.estBatteryRange}-${state.idealBatteryRange} km.`
  switch (state.chargingState) {
    case 'Charging': {
      const timeToFull = toHoursAndMinutes(state.timeToFullCharge)
      return [`Charging at ${state.chargerPower} kW, complete in ${timeToFull}. ${range}`]
    }
    case 'Complete':
      return [`Fully charged. ${range}`]
    default:
      return [`Disconnected. ${range}`]
  }
}

function toHoursAndMinutes(value) {
  const hours = Math.floor(value)
  const minutes = Math.round((value - hours) * 60)
  let msg = ''

  if (hours > 0) {
    msg = hours + ' h'
  }
  if (minutes > 0) {
    msg += (hours ? ' ' : '') + minutes + ' min'
  }
  return msg
}

function location(state) {
  if (state.latitude && state.longitude) {
    const inKnownPlace = isInKnownPlace(state.latitude, state.longitude)
    return inKnownPlace ? `${inKnownPlace.name}` : 'on the road';
  } else {
    return 'unknown'
  }
}

function isInKnownPlace(latitude, longitude) {
  return _.find(knownPlaces, place => geolib.getDistance({latitude, longitude}, place) < place.radius)
}

async function climateState(vehicle) {
  const state = await vehicle.climateState()
  return [`Inside temperature: ${state.insideTemp == null ? "-" : state.insideTemp} °C`,
	  `Outside temperature: ${state.outsideTemp == null ? "-" : state.outsideTemp} °C`,
	  `HVAC: ${state.isClimateOn == null ? "unknown" : state.isClimateOn ? "on" : "off"}`]
}

async function setHVAC(vehicle, state) {
  const result = state ? await vehicle.autoConditioningStart() : await vehicle.autoConditioningStop();
  return result.result;
}

module.exports = {
  listVehicles,
  vehicleInfo,
  vehicleWakeUp,
  setHVAC
}
